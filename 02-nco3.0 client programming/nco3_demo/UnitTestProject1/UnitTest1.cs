﻿using System;
using System.Collections;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NCo3Demo;

namespace UnitTestProject1 {
    [TestClass]
    public class UnitTest1 {
        [TestMethod]
        public void Test_GetCocdInfo() {
            SAPService sap = new SAPService();
            ArrayList cocdList = sap.GetCocdInfo("0001");

            foreach (String item in cocdList) {
                Console.WriteLine(item);
            }
        }

        [TestMethod]
        public void Test_GetCocdInfo2()
        {
            SAPService sap = new SAPService();
            ArrayList cocdList = sap.GetCocdInfo2("0001");

            foreach (String item in cocdList) {
                Console.WriteLine(item);
            }
        }

        [TestMethod]
        public void Test_ListFunctionParameters()
        {
            SAPService sap = new SAPService();
            sap.ListFunctionParameters("RFC_READ_TABLE");
        }
    }
}
