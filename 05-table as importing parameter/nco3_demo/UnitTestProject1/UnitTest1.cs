﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NCo3Demo;
using System.Data;

namespace UnitTestProject1 {
    [TestClass]
    public class UnitTest1 {
        [TestMethod]
        public void Test_ReadSKA1Table() {
            SAPService sap = new SAPService();
            DataTable cocdList = sap.ReadSKA1Table();

            DataTableHelper.Print(cocdList);
        }       
    }
}
