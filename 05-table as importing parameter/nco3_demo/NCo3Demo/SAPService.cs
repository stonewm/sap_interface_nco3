﻿using SAP.Middleware.Connector;
using System.Data;

namespace NCo3Demo {
    public class SAPService {

        public DataTable ReadSKA1Table() {
            // purpose: 
            // read table SKA1 with criteria: KTOPL = Z900            

            RfcDestination dest = DestinationProvider.GetSAPDestination();
            IRfcFunction fm = dest.Repository.CreateFunction("RFC_READ_TABLE");

            // 填写函数的参数            
            fm.SetValue("QUERY_TABLE", "SKA1");
            fm.SetValue("DELIMITER", "~");

            // OPTIONS parameter (IRfcTable)
            IRfcTable options = fm.GetTable("OPTIONS");
            options.Append(); // create a new row
            options.SetValue("TEXT", "KTOPL = 'Z900' ");

            // FIELDS parameter(IRfcTable)
            IRfcTable fields = fm.GetTable("FIELDS");
            fields.Append();
            fields.CurrentRow.SetValue("FIELDNAME", "KTOPL");
            fields.Append();
            fields.CurrentRow.SetValue("FIELDNAME", "SAKNR");

            fm.Invoke(dest);

            // DATA table paramter (output)
            IRfcTable data = fm.GetTable("DATA");
            DataTable dTable = SAPUtils.ToDataTable(data);

            return dTable;
        }
    }
}
