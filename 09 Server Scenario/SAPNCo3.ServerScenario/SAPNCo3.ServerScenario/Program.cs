﻿using SAP.Middleware.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAPNCo3.ServerScenario
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Client configuration
            RfcDestinationManager.RegisterDestinationConfiguration(new SAPDestininationConfig());
            
            // Server Configuration
            RfcServerManager.RegisterServerConfiguration(new RFCServerConfig());
            
            // Function module handlers
            Type[] handlers = new Type[1] { typeof(FunctionModuleHandler) };
            
            // Create RFC Server
            RfcServer server = RfcServerManager.GetServer("PRD_000", handlers);
            
            // Start server
            server.Start();
            
            // 等待client发起调用，指导用户按下 X 键
            Console.WriteLine("Server has been started. Press X to exit.\n");
            while (true) {
                if (Console.ReadLine().Equals("X"))
                    break;
            }
            // Server shut down
            server.Shutdown(true);            
        }
    }
}
