﻿using SAP.Middleware.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SAPNCo3.ServerScenario
{
    public class FunctionModuleHandler
    {
        [RfcServerFunction(Name = "STFC_CONNECTION")]
        public static void StfcConnection(RfcServerContext context, IRfcFunction function)
        {
            Console.WriteLine($"Received function call {function.Metadata.Name} from system {context.SystemAttributes.SystemID}.");

            // 从ABAP获取 import 参数
            String reqtext = function.GetString("REQUTEXT");
            Console.WriteLine($"REQUTEXT = {reqtext}\n");

            // 设置 export 参数
            function.SetValue("ECHOTEXT", reqtext);
            function.SetValue("RESPTEXT", "从RFC服务器返回的消息！");
        }
    }
}
