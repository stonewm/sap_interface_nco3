﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAP.Middleware.Connector;

namespace SAPNCo3.ServerScenario
{
    public class SAPDestininationConfig : IDestinationConfiguration
    {
        public event RfcDestinationManager.ConfigurationChangeHandler ConfigurationChanged;

        public bool ChangeEventsSupported()
        {
            return false;
        }

        public RfcConfigParameters GetParameters(string destinationName)
        {
            if ("ECC".Equals(destinationName)) {
                RfcConfigParameters parms = new RfcConfigParameters();

                parms.Add(RfcConfigParameters.AppServerHost, "sapecc6");  // or ip address
                parms.Add(RfcConfigParameters.SystemNumber, "00");        // instance number
                parms.Add(RfcConfigParameters.SystemID, "D01");
                parms.Add(RfcConfigParameters.User, "STONE");
                parms.Add(RfcConfigParameters.Password, "w123456");
                parms.Add(RfcConfigParameters.Client, "001");
                parms.Add(RfcConfigParameters.Language, "EN");
                parms.Add(RfcConfigParameters.PoolSize, "5");

                return parms;
            }
            else {
                return null;
            }
        }
    }

    public class RFCServerConfig : IServerConfiguration
    {
        public event RfcServerManager.ConfigurationChangeHandler ConfigurationChanged;

        public bool ChangeEventsSupported()
        {
            return false;
        }

        public RfcConfigParameters GetParameters(string serverName)
        {
            if ("PRD_000".Equals(serverName)) {
                RfcConfigParameters parms = new RfcConfigParameters();

                parms.Add(RfcConfigParameters.RepositoryDestination, "ECC");
                parms.Add(RfcConfigParameters.GatewayHost, "sapecc6");
                parms.Add(RfcConfigParameters.GatewayService, "sapgw00");
                parms.Add(RfcConfigParameters.ProgramID, "RFCSERVER");
                parms.Add(RfcConfigParameters.ConnectionCount, "5");

                return parms;
            }
            else {
                return null;
            }
        }
    }
}
