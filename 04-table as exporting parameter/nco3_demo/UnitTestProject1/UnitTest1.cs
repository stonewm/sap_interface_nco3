﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NCo3Demo;
using System.Data;

namespace UnitTestProject1 {
    [TestClass]
    public class UnitTest1 {
        [TestMethod]
        public void Test_GetCocdList() {
            SAPService sap = new SAPService();
            DataTable cocdList = sap.GetCocdList();

            DataTableHelper.Print(cocdList);
        }       
    }
}
