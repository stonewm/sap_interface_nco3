﻿using SAP.Middleware.Connector;
using System.Data;

namespace NCo3Demo {
    public class SAPService {

        public DataTable GetCocdList() { 
            DataTable cocdDataTable = null;

            RfcDestination dest = DestinationProvider.GetSAPDestination();
            IRfcFunction fm = dest.Repository.CreateFunction("BAPI_COMPANYCODE_GETLIST");
            fm.Invoke(dest);

            IRfcTable cocdRfcTable = fm.GetTable("COMPANYCODE_LIST");
            cocdDataTable = SAPUtils.ToDataTable(cocdRfcTable);

            return cocdDataTable;
        }
    }
}
