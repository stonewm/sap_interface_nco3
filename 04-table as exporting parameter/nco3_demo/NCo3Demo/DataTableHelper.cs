﻿using System;
using System.Data;

namespace NCo3Demo {
    public class DataTableHelper {
        public static void Print(DataTable dataTable) {
            foreach (DataRow row in dataTable.Rows) {
                foreach (DataColumn col in dataTable.Columns) {
                    System.Console.Write(row[col].ToString() + "\t");
                }
                Console.WriteLine();
            }
        }
    }
}
