﻿using SAP.Middleware.Connector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NCoExceptions {
    public class ExceptionDemo {
        public ArrayList GetCocdInfo(String cocd)
        {
            var list = new ArrayList();
            try {
                RfcDestination dest = DestinationProvider.GetSAPDestination();
                RfcRepository repository = dest.Repository;

                IRfcFunction fm = repository.CreateFunction("BAPI_COMPANYCODE_GETDETAIL");
                fm.SetValue("COMPANYCODEID", cocd); // fill parameter
                fm.Invoke(dest); // call function

                // BAPI_COMPANYCODE_GETDETAIL returns a structure named COMPANYCODE_DETAIL
                // which contains the information of the company code
                IRfcStructure cocdDetail = fm.GetStructure("COMPANYCODE_DETAIL");
                list = SAPUtils.ToArrayList(cocdDetail); // convert to ArrayList
            }
            catch (RfcCommunicationException ex) {
                // network problem
                System.Console.WriteLine(ex.ToString());                
            }
            catch (RfcLogonException ex) {
                // user could not log on
                System.Console.WriteLine(ex.ToString());
            }
            catch (RfcAbapBaseException ex) {
                // ABAP excpeption
                System.Console.WriteLine(ex.ToString());
            }

            return list;
        }
    }
}
