﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NCoExceptions;

namespace UnitTestProject1 {
    [TestClass]
    public class UnitTest1 {
        [TestMethod]
        public void Test_GetCocdInfo()
        {
            var sap = new ExceptionDemo();
            var cocdDetails = sap.GetCocdInfo("Z900");

            foreach(var item in cocdDetails) {
                System.Console.WriteLine(item);
            }            
        }
    }
}
