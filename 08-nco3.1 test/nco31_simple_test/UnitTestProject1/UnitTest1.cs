﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NCo3Hello;
using SAP.Middleware.Connector;

namespace UnitTestProject1 {
    [TestClass]    
    public class TestRfcConfig {
        [TestMethod]
        public void Test_Get_Destination() {
            SAPService sapService = new SAPService();
            RfcDestination dest = sapService.GetDestination();

            dest.Ping();
        }

        [TestMethod]
        public void Test_Get_Destination_from_Name() {
            SAPService sapService = new SAPService();
            RfcDestination dest = sapService.GetDestinationFromName("ECC");

            dest.Ping();
        }
    }
}

