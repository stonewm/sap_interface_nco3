﻿using SAP.Middleware.Connector;
using System;

namespace NCo3Hello {
    public class DestinationConfig : IDestinationConfiguration {
        public event RfcDestinationManager.ConfigurationChangeHandler ConfigurationChanged;

        public bool ChangeEventsSupported() {
            return false; 
        }

        public RfcConfigParameters GetParameters(string destinationName) {
            // get logon parameteres according to destinationName
            // the following is SAP logon paramters for 'ECC'
            if ("ECC".Equals(destinationName)) {
                RfcConfigParameters configParams = new RfcConfigParameters();
                configParams.Add(RfcConfigParameters.AppServerHost, "sapecc6");
                configParams.Add(RfcConfigParameters.SystemNumber, "00"); // instance number
                configParams.Add(RfcConfigParameters.SystemID, "D01");

                configParams.Add(RfcConfigParameters.User, "STONE");
                configParams.Add(RfcConfigParameters.Password, "w123456");
                configParams.Add(RfcConfigParameters.Client, "001");
                configParams.Add(RfcConfigParameters.Language, "EN");
                configParams.Add(RfcConfigParameters.PoolSize, "5");

                return configParams;
            }
            else {
                return null;
            }
        }
    }
}
